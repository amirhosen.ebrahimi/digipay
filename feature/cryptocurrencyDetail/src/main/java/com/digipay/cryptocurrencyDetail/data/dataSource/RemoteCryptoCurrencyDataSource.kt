package com.digipay.cryptocurrencyDetail.data.dataSource

import com.digipay.cryptocurrencyDetail.data.webApi.response.CryptoCurrencyDetailResponse
import com.digipay.cryptocurrencyDetail.data.webApi.response.CryptoCurrencyResponse
import com.digipay.cryptocurrencyDetail.data.webApi.service.CryptoCurrencyDetailService
import io.phoenix.businessmessenger.common.datasource.Readable
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import javax.inject.Inject

class RemoteCryptoCurrencyDataSource @Inject constructor(
    private val cryptoCurrencyService: CryptoCurrencyDetailService,
    private val CryptoCurrencyResponseToCryptoCurrencyDetailEntity: Mapper<CryptoCurrencyResponse, CryptoCurrencyDetailEntity>
) : RemoteCryptoCurrencyDataSourceReadable {
    override suspend fun read(input: RemoteCryptoCurrencyDataSourceReadable.Params): CryptoCurrencyDetailEntity {
        val data = cryptoCurrencyService.getCryptoCurrencyDetail(id = input.id)
        val cryptoCurrencyResponse = data.body()!!.getAsJsonObject("data").getAsJsonObject(input.id)
        return CryptoCurrencyResponseToCryptoCurrencyDetailEntity.map(
            CryptoCurrencyResponse(
                CryptoCurrencyDetailResponse.getFromJsonObject(cryptoCurrencyResponse)
            )
        )
    }
}


interface RemoteCryptoCurrencyDataSourceReadable :
    Readable.Suspendable.IO<RemoteCryptoCurrencyDataSourceReadable.Params,
            CryptoCurrencyDetailEntity> {
    data class Params(val id: String)
}
