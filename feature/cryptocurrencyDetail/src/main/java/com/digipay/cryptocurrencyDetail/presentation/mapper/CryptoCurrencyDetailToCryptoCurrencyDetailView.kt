package com.digipay.cryptocurrencyDetail.presentation.mapper

import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.presentation.entity.CryptoCurrencyDetailView
import io.phoenix.businessmessenger.common.mapper.Mapper
import javax.inject.Inject

class CryptoCurrencyDetailToCryptoCurrencyDetailView @Inject constructor() :
    Mapper<CryptoCurrencyDetail, CryptoCurrencyDetailView> {
    override fun map(first: CryptoCurrencyDetail): CryptoCurrencyDetailView {
        return CryptoCurrencyDetailView(
            id = first.id, name = first.name, des = first.des, logo = first.logo
        )
    }

}