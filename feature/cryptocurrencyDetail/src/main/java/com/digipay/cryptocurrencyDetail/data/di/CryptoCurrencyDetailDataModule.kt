package com.digipay.cryptocurrencyDetail.data.di

import com.digipay.cryptocurrencyDetail.CryptoCurrencyDetailScope
import com.digipay.cryptocurrencyDetail.data.dataSource.*
import com.digipay.cryptocurrencyDetail.data.mapper.CryptoCurrencyDetailEntityToCryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.data.mapper.CryptoCurrencyResponseToCryptoCurrencyDetailEntity
import com.digipay.cryptocurrencyDetail.data.repository.CryptoCurrencyDetailRepositoryImp
import com.digipay.cryptocurrencyDetail.data.webApi.response.CryptoCurrencyResponse
import com.digipay.cryptocurrencyDetail.data.webApi.service.CryptoCurrencyDetailService
import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.domain.repository.CryptoCurrencyDetailRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import retrofit2.Retrofit

@Module
abstract class CryptoCurrencyDetailDataModule {
    //......................Mappers...........................

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideCryptoCurrencyResponseToCryptoCurrencyDetailEntity(mapper: CryptoCurrencyResponseToCryptoCurrencyDetailEntity): Mapper<CryptoCurrencyResponse, CryptoCurrencyDetailEntity>

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideCryptoCurrencyDetailEntityToCryptoCurrencyDetail(mapper: CryptoCurrencyDetailEntityToCryptoCurrencyDetail): Mapper<CryptoCurrencyDetailEntity, CryptoCurrencyDetail>

    //......................DataSources...........................

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideRemoteCryptoCurrencyDetailDataSourceReadable(dataSource: RemoteCryptoCurrencyDataSource): RemoteCryptoCurrencyDataSourceReadable

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideLocalCryptoCurrencyDetailDataSourceReadable(dataSource: LocalCryptoCurrencyDetailDataSource): LocalCryptoCurrencyDetailDataSourceReadable

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideLocalCryptoCurrencyDetailDataSourceWritable(dataSource: LocalCryptoCurrencyDetailDataSource): LocalCryptoCurrencyDetailDataSourceWritable

    //......................Repositories...........................

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideCryptoCurrencyDetailRepository(repository: CryptoCurrencyDetailRepositoryImp): CryptoCurrencyDetailRepository

    companion object {
        @Provides
        @CryptoCurrencyDetailScope
        fun provideApiService(retrofit: Retrofit): CryptoCurrencyDetailService {
            return retrofit.create(CryptoCurrencyDetailService::class.java)
        }
    }
}