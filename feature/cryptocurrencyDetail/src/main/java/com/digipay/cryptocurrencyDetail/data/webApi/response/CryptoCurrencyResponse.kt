package com.digipay.cryptocurrencyDetail.data.webApi.response

data class CryptoCurrencyResponse(
    var cryptoCurrencyDetailResponse: CryptoCurrencyDetailResponse
)