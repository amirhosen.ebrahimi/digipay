package com.digipay.cryptocurrencyDetail.presentation.di

import androidx.lifecycle.ViewModel
import com.digipay.cryptocurrencyDetail.CryptoCurrencyDetailScope
import com.digipay.cryptocurrencyDetail.presentation.CryptoCurrencyDetailViewModel
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.phoenix.businessmessenger.common.di.AssistedSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.di.ViewModelKey

@AssistedModule
@Module(includes = [AssistedInject_CryptoCurrencyDetailAssistedModule::class])
abstract class CryptoCurrencyDetailAssistedModule {
    @Binds
    @IntoMap
    @CryptoCurrencyDetailScope
    @ViewModelKey(CryptoCurrencyDetailViewModel::class)
    abstract fun bindVMFactory(f: CryptoCurrencyDetailViewModel.Factory): AssistedSavedStateViewModelFactory<out ViewModel>

}