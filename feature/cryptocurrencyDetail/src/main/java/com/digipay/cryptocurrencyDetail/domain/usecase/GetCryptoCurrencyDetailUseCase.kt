package com.digipay.cryptocurrencyDetail.domain.usecase


import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.domain.repository.CryptoCurrencyDetailRepository
import io.phoenix.businessmessenger.common.thread.IoDispatcher
import io.phoenix.businessmessenger.common.usecase.FlowUseCase
import io.phoenix.businessmessenger.data.entity.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetCryptoCurrencyDetailUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val cryptoCurrencyDetailRepository: CryptoCurrencyDetailRepository
) :
    FlowUseCase<GetCryptoCurrencyDetailUseCase.Params, CryptoCurrencyDetail>(dispatcher) {

    override fun execute(parameters: Params): Flow<Resource<CryptoCurrencyDetail>> {
        return cryptoCurrencyDetailRepository.getCryptoCurrencyDetail(
            parameters.id
        ).map {
            Resource.Success(it)
        }
    }

    data class Params(
        val id: String
    )


}