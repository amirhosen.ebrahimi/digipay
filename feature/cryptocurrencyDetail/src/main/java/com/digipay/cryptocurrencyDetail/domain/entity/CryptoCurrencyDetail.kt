package com.digipay.cryptocurrencyDetail.domain.entity

data class CryptoCurrencyDetail(
    val id: Int, val name: String, val des: String, val logo: String
)