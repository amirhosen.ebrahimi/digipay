package com.digipay.cryptocurrencyDetail.domain.repository

import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import kotlinx.coroutines.flow.Flow

interface CryptoCurrencyDetailRepository {
      fun getCryptoCurrencyDetail(id: String): Flow<CryptoCurrencyDetail>
}