package com.digipay.cryptocurrencyDetail.presentation


import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.domain.usecase.GetCryptoCurrencyDetailUseCase
import com.digipay.cryptocurrencyDetail.presentation.entity.CryptoCurrencyDetailView
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import io.phoenix.businessmessenger.common.di.AssistedSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.Resource
import io.phoenix.businessmessenger.data.entity.map
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class CryptoCurrencyDetailViewModel @AssistedInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val getCryptoCurrencyDetailUseCase: GetCryptoCurrencyDetailUseCase,
    private val mapper: Mapper<CryptoCurrencyDetail, CryptoCurrencyDetailView>
) : ViewModel() {

    val id = savedStateHandle.get<String>("id")


    private val _cryptoCurrencyDetail = MutableSharedFlow<Resource<CryptoCurrencyDetailView>>()
    val cryptoCurrencyDetail: SharedFlow<Resource<CryptoCurrencyDetailView>>
        get() = _cryptoCurrencyDetail


    fun getCryptoCurrencyDetailRequest() {
        id?.let {
            viewModelScope.launch {
                _cryptoCurrencyDetail.emit(Resource.Loading(true))
                getCryptoCurrencyDetailUseCase(GetCryptoCurrencyDetailUseCase.Params(it)).map {
                    it.map(mapper)
                }.collect {
                    _cryptoCurrencyDetail.emit(it)
                }
            }
        }

    }

    @AssistedInject.Factory
    interface Factory : AssistedSavedStateViewModelFactory<CryptoCurrencyDetailViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): CryptoCurrencyDetailViewModel
    }

}
