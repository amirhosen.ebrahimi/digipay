package com.digipay.cryptocurrencyDetail.presentation.di

import com.digipay.cryptocurrencyDetail.CryptoCurrencyDetailScope
import com.digipay.cryptocurrencyDetail.presentation.entity.CryptoCurrencyDetailView
import com.digipay.cryptocurrencyDetail.presentation.mapper.CryptoCurrencyDetailToCryptoCurrencyDetailView
import dagger.Binds
import dagger.Module
import io.phoenix.businessmessenger.common.di.InjectingSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.di.ViewModelFactory
import io.phoenix.businessmessenger.common.mapper.Mapper

@Module
abstract class CryptoCurrencyDetailPresentationModule {
    //......................Mappers...........................

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun provideCryptoCurrencyDetailToCryptoCurrencyDetailView(mapper: CryptoCurrencyDetailToCryptoCurrencyDetailView): Mapper<com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail, CryptoCurrencyDetailView>

    @Binds
    @CryptoCurrencyDetailScope
    abstract fun bindViewModelFactory(mapper: InjectingSavedStateViewModelFactory): ViewModelFactory

}