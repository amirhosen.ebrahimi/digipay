package com.digipay.cryptocurrencyDetail.data.dataSource

import io.phoenix.businessmessenger.common.datasource.Readable
import io.phoenix.businessmessenger.common.datasource.Writable
import io.phoenix.businessmessenger.data.dao.CryptoCurrencyDetailDao
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import javax.inject.Inject

class LocalCryptoCurrencyDetailDataSource @Inject constructor(private val cryptoCurrencyDetailDao: CryptoCurrencyDetailDao) :
    LocalCryptoCurrencyDetailDataSourceReadable, LocalCryptoCurrencyDetailDataSourceWritable {
    override suspend fun read(input: LocalCryptoCurrencyDetailDataSourceReadable.Params): CryptoCurrencyDetailEntity {
        return cryptoCurrencyDetailDao.getById(input.id)
    }

    override suspend fun write(input: LocalCryptoCurrencyDetailDataSourceWritable.Params) {
        cryptoCurrencyDetailDao.insert(input.CryptoCurrencyDetailEntity)
    }

}

interface LocalCryptoCurrencyDetailDataSourceReadable :
    Readable.Suspendable.IO<LocalCryptoCurrencyDetailDataSourceReadable.Params,
            CryptoCurrencyDetailEntity> {
    data class Params(val id: String)
}

interface LocalCryptoCurrencyDetailDataSourceWritable :
    Writable.Suspendable.IO<LocalCryptoCurrencyDetailDataSourceWritable.Params,
            Unit> {
    data class Params(val CryptoCurrencyDetailEntity: CryptoCurrencyDetailEntity)
}

