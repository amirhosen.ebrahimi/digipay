package com.digipay.cryptocurrencyDetail

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class CryptoCurrencyDetailScope