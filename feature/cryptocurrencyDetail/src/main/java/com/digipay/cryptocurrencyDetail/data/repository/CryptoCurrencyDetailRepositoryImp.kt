package com.digipay.cryptocurrencyDetail.data.repository

import com.digipay.cryptocurrencyDetail.data.dataSource.LocalCryptoCurrencyDetailDataSourceReadable
import com.digipay.cryptocurrencyDetail.data.dataSource.LocalCryptoCurrencyDetailDataSourceWritable
import com.digipay.cryptocurrencyDetail.data.dataSource.RemoteCryptoCurrencyDataSourceReadable
import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import com.digipay.cryptocurrencyDetail.domain.repository.CryptoCurrencyDetailRepository
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.common.networking.networkWithCache
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CryptoCurrencyDetailRepositoryImp @Inject constructor(
    private val localCryptoCurrencyDetailDataSourceReadable: LocalCryptoCurrencyDetailDataSourceReadable,
    private val localCryptoCurrencyDetailDataSourceWritable: LocalCryptoCurrencyDetailDataSourceWritable,
    private val remoteCryptoCurrencyDataSourceReadable: RemoteCryptoCurrencyDataSourceReadable,
    private val CryptoCurrencyDetailEntityToCryptoCurrencyDetail: Mapper<CryptoCurrencyDetailEntity, CryptoCurrencyDetail>
) : CryptoCurrencyDetailRepository {
    override fun getCryptoCurrencyDetail(id: String): Flow<CryptoCurrencyDetail> {
        return flow {
            val request = networkWithCache(
                createCall = {
                    remoteCryptoCurrencyDataSourceReadable.read(
                        RemoteCryptoCurrencyDataSourceReadable.Params(id)
                    )
                },
                loadFromLocal = {
                    localCryptoCurrencyDetailDataSourceReadable.read(
                        LocalCryptoCurrencyDetailDataSourceReadable.Params(id)
                    )
                },
                shouldFetch = {
                    localCryptoCurrencyDetailDataSourceReadable.read(
                        LocalCryptoCurrencyDetailDataSourceReadable.Params(id)
                    ) == null
                },
                saveCallResult = {
                    localCryptoCurrencyDetailDataSourceWritable.write(
                        LocalCryptoCurrencyDetailDataSourceWritable.Params(
                            it
                        )
                    )
                }
            )
            emit(CryptoCurrencyDetailEntityToCryptoCurrencyDetail.map(request))
        }

    }

}