package com.digipay.cryptocurrencyDetail.data.webApi.service

import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CryptoCurrencyDetailService {
    @GET("/v1/cryptocurrency/info")
    suspend fun getCryptoCurrencyDetail(
        @Header("X-CMC_PRO_API_KEY") token: String = apiKey,
        @Query("id") id: String,
    ): Response<JsonObject>

    companion object {
        const val apiKey =
            "4ca1b9fd-c5c2-4d72-8fc5-9fa7afbcd38c"
    }
}