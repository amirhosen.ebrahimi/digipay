package com.digipay.cryptocurrencyDetail.presentation

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.digipay.cryptocurrencyDetail.R
import com.digipay.cryptocurrencyDetail.databinding.FragmentCryptoCurrencyDetailDetailBinding
import dagger.android.support.DaggerFragment
import io.phoenix.businessmessenger.common.di.ViewModelFactory
import io.phoenix.businessmessenger.common.sdkextentions.bindings.imageUrl
import io.phoenix.businessmessenger.common.sdkextentions.properties.viewBinding
import io.phoenix.businessmessenger.data.entity.onError
import io.phoenix.businessmessenger.data.entity.onSuccess
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class CryptoCurrencyDetailFragment :
    DaggerFragment(R.layout.fragment_crypto_currency_detail_detail) {

    @Inject
    lateinit var abstractFactory: ViewModelFactory

    private val binding by viewBinding(FragmentCryptoCurrencyDetailDetailBinding::bind)
    private val viewModel by viewModels<CryptoCurrencyDetailViewModel> {
        abstractFactory.create(
            this,
            arguments
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getCryptoCurrencyDetailRequest()
        binding.apply {
            toolbar.setupWithNavController(findNavController())
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.cryptoCurrencyDetail.collect { it ->
                    binding.errorTextView.isVisible = it.isError()
                    binding.progressBar.isVisible = it.isLoading()
                    binding.retryButton.isVisible = it.isError()
                    binding.materialCardViewDetail.isVisible =
                        ((!it.isLoading()) && (!it.isError()))
                    binding.materialCardView.isVisible = ((!it.isLoading()) && (!it.isError()))
                    it.onSuccess {
                        binding.nameText.text = it.name
                        binding.desText.text = it.des
                        binding.headerImage.imageUrl(it.logo)
                    }
                    it.onError {
                        Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        binding.retryButton.setOnClickListener {
            viewModel.getCryptoCurrencyDetailRequest()
        }
    }
}
