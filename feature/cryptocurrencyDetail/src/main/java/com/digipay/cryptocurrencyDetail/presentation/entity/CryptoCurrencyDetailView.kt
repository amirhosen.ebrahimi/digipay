package com.digipay.cryptocurrencyDetail.presentation.entity

data class CryptoCurrencyDetailView(
    val id: Int,
    val name: String,
    val des: String,
    val logo: String
)