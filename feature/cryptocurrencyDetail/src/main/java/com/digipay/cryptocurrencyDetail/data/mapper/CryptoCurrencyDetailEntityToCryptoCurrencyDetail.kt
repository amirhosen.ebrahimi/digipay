package com.digipay.cryptocurrencyDetail.data.mapper

import com.digipay.cryptocurrencyDetail.domain.entity.CryptoCurrencyDetail
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import javax.inject.Inject


class CryptoCurrencyDetailEntityToCryptoCurrencyDetail @Inject constructor() :
    Mapper<CryptoCurrencyDetailEntity, CryptoCurrencyDetail> {
    override fun map(first: CryptoCurrencyDetailEntity): CryptoCurrencyDetail {
        return CryptoCurrencyDetail(
            id = first.id,
            name = first.name,
            des = first.des,
            logo = first.logo
        )
    }

}