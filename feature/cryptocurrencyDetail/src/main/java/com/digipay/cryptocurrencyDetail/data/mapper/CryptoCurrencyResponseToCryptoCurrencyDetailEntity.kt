package com.digipay.cryptocurrencyDetail.data.mapper

import com.digipay.cryptocurrencyDetail.data.webApi.response.CryptoCurrencyResponse
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import javax.inject.Inject

class CryptoCurrencyResponseToCryptoCurrencyDetailEntity @Inject constructor() :
    Mapper<CryptoCurrencyResponse, CryptoCurrencyDetailEntity> {
    override fun map(first: CryptoCurrencyResponse): CryptoCurrencyDetailEntity {
        return CryptoCurrencyDetailEntity(
            id = first.cryptoCurrencyDetailResponse.id,
            name = first.cryptoCurrencyDetailResponse.name,
            logo = first.cryptoCurrencyDetailResponse.logo,
            des = first.cryptoCurrencyDetailResponse.description
        )
    }
}
