package com.digipay.cryptocurrencyDetail.data.webApi.response

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName


data class CryptoCurrencyDetailResponse(

    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("symbol") var symbol: String,
    @SerializedName("description") var description: String,
    @SerializedName("logo") var logo: String,
) {
    companion object {
        fun getFromJsonObject(
            jsonObject: JsonObject
        ): CryptoCurrencyDetailResponse {
            return CryptoCurrencyDetailResponse(
                id = jsonObject.get("id").asInt,
                name = jsonObject.get("name").asString,
                logo = jsonObject.get("logo").asString,
                symbol = jsonObject.get("symbol").asString,
                description = jsonObject.get("description").asString,
            )
        }
    }

}