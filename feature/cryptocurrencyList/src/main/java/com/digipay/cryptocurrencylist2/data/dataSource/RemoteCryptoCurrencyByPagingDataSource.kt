package com.digipay.cryptocurrencylist2.data.dataSource

import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import com.digipay.cryptocurrencylist2.data.webApi.service.CryptoCurrencyService
import io.phoenix.businessmessenger.common.datasource.Readable
import io.phoenix.businessmessenger.data.entity.BaseResponse
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import retrofit2.Response
import javax.inject.Inject

class RemoteCryptoCurrencyByPagingDataSource @Inject constructor(private val cryptoCurrencyService: CryptoCurrencyService) :
    RemoteCryptoCurrencyByPagingDataSourceReadable {
    override suspend fun read(input: RemoteCryptoCurrencyByPagingDataSourceReadable.Params): Response<BaseResponse<List<Data>>> {
        return cryptoCurrencyService.getCryptoCurrencyByPaging(
            page = input.cryptoCurrencyRemoteKeyEntity.nextPageKey,
            size = input.cryptoCurrencyRemoteKeyEntity.limit
        )
    }
}

interface RemoteCryptoCurrencyByPagingDataSourceReadable :
    Readable.Suspendable.IO<RemoteCryptoCurrencyByPagingDataSourceReadable.Params, @JvmSuppressWildcards Response<BaseResponse<List<Data>>>> {
    data class Params(val cryptoCurrencyRemoteKeyEntity: CryptoCurrencyRemoteKeyEntity)
}