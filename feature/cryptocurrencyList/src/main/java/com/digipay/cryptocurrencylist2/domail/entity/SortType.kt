package com.digipay.cryptocurrencylist2.domail.entity

object SortType {
    const val none = "0"
    const val marketCap = "1"
    const val price = "2"
    const val name = "3"

}

