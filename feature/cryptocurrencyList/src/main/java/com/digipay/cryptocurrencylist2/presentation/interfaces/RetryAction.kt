package com.digipay.cryptocurrencylist2.presentation.interfaces

interface RetryAction {
    fun retryClick()
}