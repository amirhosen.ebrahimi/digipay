package com.digipay.cryptocurrencylist2.data.dataSource

import androidx.paging.PagingSource
import com.digipay.cryptocurrencylist2.domail.entity.SortType
import io.phoenix.businessmessenger.common.datasource.Deletable
import io.phoenix.businessmessenger.common.datasource.Readable
import io.phoenix.businessmessenger.common.datasource.Writable
import io.phoenix.businessmessenger.data.dao.CryptoCurrencyDao
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import javax.inject.Inject

class LocalCryptoCurrencyDataSource @Inject constructor(private val cryptoCurrencyDao: CryptoCurrencyDao) :
    LocalCryptoCurrencyDataSourceReadable, LocalCryptoCurrencyDataSourceWritable,
    LocalCryptoCurrencyDataSourceDeletable {
    override fun read(input: LocalCryptoCurrencyDataSourceReadable.Params): PagingSource<Int, CryptoCurrencyEntity> {
        when (input.sortType) {
            SortType.name -> {
                return cryptoCurrencyDao.getAllByName()
            }
            SortType.marketCap -> {
                return cryptoCurrencyDao.getAllByMarketCap()

            }
            SortType.price -> {
                return cryptoCurrencyDao.getAllByPrice()

            }
            else -> {
                return cryptoCurrencyDao.getAll()
            }
        }
    }

    override suspend fun write(input: LocalCryptoCurrencyDataSourceWritable.Params) {
        cryptoCurrencyDao.insertAll(input.list)
    }

    override fun delete(input: Unit) {
        cryptoCurrencyDao.deleteAll()
    }

}

interface LocalCryptoCurrencyDataSourceReadable :
    Readable.IO<LocalCryptoCurrencyDataSourceReadable.Params, @JvmSuppressWildcards PagingSource<Int, CryptoCurrencyEntity>> {
    data class Params(val sortType: String)
}

interface LocalCryptoCurrencyDataSourceWritable :
    Writable.Suspendable<@JvmSuppressWildcards LocalCryptoCurrencyDataSourceWritable.Params> {
    data class Params(val list: List<CryptoCurrencyEntity>)
}

interface LocalCryptoCurrencyDataSourceDeletable : Deletable<Unit>


