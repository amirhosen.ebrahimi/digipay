package com.digipay.cryptocurrencylist2.presentation.di

import androidx.lifecycle.ViewModel
import com.digipay.cryptocurrencylist2.CryptoCurrencyScope
import com.digipay.cryptocurrencylist2.presentation.CryptoCurrencyViewModel
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.phoenix.businessmessenger.common.di.AssistedSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.di.ViewModelKey

@AssistedModule
@Module(includes = [AssistedInject_CryptoCurrencyAssistedModule::class])
abstract class CryptoCurrencyAssistedModule {
    @Binds
    @IntoMap
    @CryptoCurrencyScope
    @ViewModelKey(CryptoCurrencyViewModel::class)
    abstract fun bindVMFactory(f: CryptoCurrencyViewModel.Factory): AssistedSavedStateViewModelFactory<out ViewModel>

}