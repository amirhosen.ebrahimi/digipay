package com.digipay.cryptocurrencylist2.presentation

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.paging.LoadState
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.digipay.cryptocurrencylist2.R
import com.digipay.cryptocurrencylist2.databinding.FragmentCryptoCurrencyBinding
import com.digipay.cryptocurrencylist2.domail.entity.SortType
import com.digipay.cryptocurrencylist2.presentation.adapter.CryptoCurrencyListAdapter
import com.digipay.cryptocurrencylist2.presentation.adapter.CryptoCurrencyLoadStateAdapter
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import com.digipay.cryptocurrencylist2.presentation.interfaces.CryptoCurrencyListAction
import com.digipay.cryptocurrencylist2.presentation.interfaces.RetryAction
import dagger.android.support.DaggerFragment
import io.phoenix.businessmessenger.common.di.ViewModelFactory
import io.phoenix.businessmessenger.common.sdkextentions.navigation.DefaultNavOptions
import io.phoenix.businessmessenger.common.sdkextentions.properties.autoCleared
import io.phoenix.businessmessenger.common.sdkextentions.properties.viewBinding
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


class CryptoCurrencyFragment : DaggerFragment(R.layout.fragment_crypto_currency),
    SwipeRefreshLayout.OnRefreshListener, CompoundButton.OnCheckedChangeListener,
    RetryAction, CryptoCurrencyListAction {
    @Inject
    lateinit var abstractFactory: ViewModelFactory

    private var adapter by autoCleared<CryptoCurrencyListAdapter>()

    private val binding by viewBinding(FragmentCryptoCurrencyBinding::bind)
    private val viewModel by viewModels<CryptoCurrencyViewModel> {
        abstractFactory.create(
            this,
            arguments
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = CryptoCurrencyListAdapter(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            toolbar.setupWithNavController(findNavController())
            swipeRefresh.setOnRefreshListener(this@CryptoCurrencyFragment)
            list.adapter = adapter.withLoadStateHeaderAndFooter(
                header = CryptoCurrencyLoadStateAdapter(
                    this@CryptoCurrencyFragment
                ),
                footer = CryptoCurrencyLoadStateAdapter(
                    this@CryptoCurrencyFragment
                )
            )
        }
        binding.nameSort.setOnCheckedChangeListener(this)
        binding.defaultSort.setOnCheckedChangeListener(this)
        binding.marketCapSort.setOnCheckedChangeListener(this)
        binding.priceSort.setOnCheckedChangeListener(this)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.cryptoCurrencyList.collectLatest {
                    binding.swipeRefresh.isRefreshing = false
                    adapter.submitData(it)
                }
                adapter.loadStateFlow
                    .distinctUntilChangedBy { it.refresh }
                    .filter { it.refresh is LoadState.NotLoading }
                    .collect { binding.list.scrollToPosition(0) }
            }

        }
    }

    override fun onRefresh() {
        viewModel.rGetLastCryptoCurrency()
    }

    override fun retryClick() {
        adapter.retry()
    }

    override fun onItemClick(cryptoCurrency: CryptoCurrencyView) {
        findNavController().navigate(
            Uri.parse("digipay://CryptoCurrencyDetail/?id=" + cryptoCurrency.id), DefaultNavOptions
        )
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        when (p0?.id) {
            R.id.price_sort -> {
                viewModel.rGetCryptoCurrency(SortType.price)
            }
            R.id.default_sort -> {
                viewModel.rGetCryptoCurrency(SortType.none)
            }
            R.id.name_sort -> {
                viewModel.rGetCryptoCurrency(SortType.name)
            }
            R.id.marketCap_sort -> {
                viewModel.rGetCryptoCurrency(SortType.marketCap)
            }
        }
    }
}

