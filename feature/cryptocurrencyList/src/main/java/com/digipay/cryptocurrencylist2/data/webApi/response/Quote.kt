package com.digipay.cryptocurrencylist2.data.webApi.response

import com.google.gson.annotations.SerializedName

data class Quote(

    @SerializedName("USD") val usd: USD
)