package com.digipay.cryptocurrencylist2.data.webApi.service

import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import io.phoenix.businessmessenger.data.entity.BaseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CryptoCurrencyService {
    @GET("/v1/cryptocurrency/listings/latest")
    suspend fun getCryptoCurrencyByPaging(
        @Header("X-CMC_PRO_API_KEY") token: String = apiKey,
        @Query("start") page: Int,
        @Query("limit") size: Int,
        @Query("convert") convert: String = "USD"
    ): Response<BaseResponse<List<Data>>>

    companion object {
        const val apiKey =
            "4ca1b9fd-c5c2-4d72-8fc5-9fa7afbcd38c"
    }
}

