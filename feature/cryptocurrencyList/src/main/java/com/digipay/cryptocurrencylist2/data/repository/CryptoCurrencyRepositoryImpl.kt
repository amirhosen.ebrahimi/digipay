package com.digipay.cryptocurrencylist2.data.repository

import androidx.paging.*
import com.digipay.cryptocurrencylist2.data.dataSource.LocalCryptoCurrencyDataSourceReadable
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.domail.repository.CryptoCurrencyRepository
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

class CryptoCurrencyRepositoryImpl @Inject constructor(
    private val pageKeyedRemoteMediator: PageKeyedRemoteMediator,
    private val localCryptoCurrencyDataSourceReadable: LocalCryptoCurrencyDataSourceReadable,
    private val cryptoCurrencyEntityToCryptoCurrency: Mapper<CryptoCurrencyEntity, CryptoCurrency>
) : CryptoCurrencyRepository {


    @ExperimentalPagingApi
    override fun getCryptoCurrency(
        pagingConfig: PagingConfig,
        sortType: String
    ): Flow<PagingData<CryptoCurrency>> {
        return Pager(
            config = pagingConfig,
            remoteMediator = pageKeyedRemoteMediator,
            pagingSourceFactory = {
                localCryptoCurrencyDataSourceReadable.read(
                    LocalCryptoCurrencyDataSourceReadable.Params(sortType)
                )
            }
        ).flow.mapLatest { pagingData ->
            pagingData.map { entity ->
                cryptoCurrencyEntityToCryptoCurrency.map(entity)
            }
        }
    }


}