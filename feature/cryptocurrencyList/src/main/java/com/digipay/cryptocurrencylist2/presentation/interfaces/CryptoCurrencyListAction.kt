package com.digipay.cryptocurrencylist2.presentation.interfaces

import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView


interface CryptoCurrencyListAction {
    fun onItemClick(cryptoCurrency: CryptoCurrencyView)
}