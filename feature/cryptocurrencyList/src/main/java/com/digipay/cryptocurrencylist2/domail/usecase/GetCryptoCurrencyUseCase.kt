package com.digipay.cryptocurrencylist2.domail.usecase

import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.digipay.cryptocurrencylist2.CryptoCurrencyScope
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.domail.entity.SortType
import com.digipay.cryptocurrencylist2.domail.repository.CryptoCurrencyRepository
import io.phoenix.businessmessenger.common.thread.IoDispatcher
import io.phoenix.businessmessenger.common.usecase.PagingUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@CryptoCurrencyScope
class GetCryptoCurrencyUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val repository: CryptoCurrencyRepository,
) : PagingUseCase<GetCryptoCurrencyUseCase.Params, PagingData<CryptoCurrency>>(dispatcher) {

    override suspend fun execute(parameters: Params): Flow<PagingData<CryptoCurrency>> {
        return repository.getCryptoCurrency(parameters.pagingConfig, parameters.sortType)
    }

    data class Params(
        val pagingConfig: PagingConfig,
        val sortType: String = SortType.none
    )

}