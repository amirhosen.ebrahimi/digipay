package com.digipay.cryptocurrencylist2.data.dataSource

import io.phoenix.businessmessenger.common.datasource.Deletable
import io.phoenix.businessmessenger.common.datasource.Readable
import io.phoenix.businessmessenger.common.datasource.Writable
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import io.phoenix.businessmessenger.data.pref.PreferenceStorage
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalCryptoCurrencyRemoteKeyDataSource @Inject constructor(private val preferenceStorage: PreferenceStorage) :
    LocalCryptoCurrencyRemoteKeyDataSourceWritable, LocalCryptoCurrencyRemoteKeyDataSourceReadable,
    LocalCryptoCurrencyRemoteKeyDataSourceDeletable {
    override suspend fun write(input: CryptoCurrencyRemoteKeyEntity) {
        preferenceStorage.saveCryptoCurrencyRemoteKey(input)
    }

    override fun read(): Flow<CryptoCurrencyRemoteKeyEntity> {
        return preferenceStorage.cryptoCurrencyRemoteKeyEntity
    }

    override fun delete(input: Unit) {
        preferenceStorage.cryptoCurrencyRemoteKeyEntity // todo
    }

}

interface LocalCryptoCurrencyRemoteKeyDataSourceDeletable :
    Deletable<Unit>

interface LocalCryptoCurrencyRemoteKeyDataSourceWritable :
    Writable.Suspendable<@JvmSuppressWildcards CryptoCurrencyRemoteKeyEntity>

interface LocalCryptoCurrencyRemoteKeyDataSourceReadable :
    Readable<@JvmSuppressWildcards Flow<CryptoCurrencyRemoteKeyEntity>>
