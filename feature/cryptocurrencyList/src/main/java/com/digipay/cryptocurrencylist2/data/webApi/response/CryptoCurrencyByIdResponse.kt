package com.digipay.cryptocurrencylist2.data.webApi.response

import com.google.gson.annotations.SerializedName

data class CryptoCurrencyByIdResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("username")
    val userName: String,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?,
    @SerializedName("thumbnail")
    val thumbnail: String?,
    @SerializedName("thumbnailUrl")
    val thumbnailUrl: Boolean,
)