package com.digipay.cryptocurrencylist2.presentation.entity

data class CryptoCurrencyView(
    val id: Int,
    val name: String,
    val symbol: String?,
    val price: String?,
    val percentChange: String?,
    val marketCap: String?
)