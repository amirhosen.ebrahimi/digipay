package com.digipay.cryptocurrencylist2.data.mapper

import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import javax.inject.Inject

class CryptoCurrencyListResponseToCryptoCurrencyEntity @Inject constructor() :
    Mapper<@JvmSuppressWildcards List<Data>, @JvmSuppressWildcards List<CryptoCurrencyEntity>> {

    override fun map(first: List<Data>): List<CryptoCurrencyEntity> {
        return first.map {
            CryptoCurrencyEntity(
                id = it.id,
                name = it.name,
                symbol = it.symbol,
                price = it.quote.usd.price.toString(),
                percentChange = it.quote.usd.percent_change_24h.toString(),
                marketCap = it.quote.usd.market_cap.toString(),
            )
        }
    }
}