package com.digipay.cryptocurrencylist2.domail.repository

import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import kotlinx.coroutines.flow.Flow

interface CryptoCurrencyRepository {
      fun getCryptoCurrency(
          pagingConfig: PagingConfig,
          sortType: String
      ): Flow<PagingData<CryptoCurrency>>
}