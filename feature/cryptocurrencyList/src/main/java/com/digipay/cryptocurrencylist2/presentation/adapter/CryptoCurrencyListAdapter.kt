package com.digipay.cryptocurrencylist2.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import com.digipay.cryptocurrencylist2.presentation.interfaces.CryptoCurrencyListAction
import io.phoenix.businessmessenger.designSystem.ColorGenerator
import io.phoenix.businessmessenger.designSystem.TextDrawable
import io.phoenix.businessmessenger.designSystem.databinding.ItemCryptoCurrencyBinding

internal class CryptoCurrencyListAdapter constructor(val cryptoCurrencyListAction: CryptoCurrencyListAction) :
    PagingDataAdapter<CryptoCurrencyView, CryptoCurrencyViewHolder>(
        PLACE_COMPARATOR
    ) {


    override fun onBindViewHolder(holder: CryptoCurrencyViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, cryptoCurrencyListAction) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CryptoCurrencyViewHolder {
        return CryptoCurrencyViewHolder(
            ItemCryptoCurrencyBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    companion object {
        private val PLACE_COMPARATOR = object : DiffUtil.ItemCallback<CryptoCurrencyView>() {
            override fun areItemsTheSame(
                oldItem: CryptoCurrencyView,
                newItem: CryptoCurrencyView
            ): Boolean =
                oldItem == newItem


            override fun areContentsTheSame(
                oldItem: CryptoCurrencyView,
                newItem: CryptoCurrencyView
            ): Boolean =
                oldItem == newItem
        }
    }
}

internal class CryptoCurrencyViewHolder(
    private val binding: ItemCryptoCurrencyBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        cryptoCurrency: CryptoCurrencyView,
        cryptoCurrencyListAction: CryptoCurrencyListAction
    ) = cryptoCurrency.let {
        binding.name.text = it.name
        binding.price.text = it.price
        binding.priceChange.text = it.price
        binding.marketCap.text = it.marketCap

        val text = if (it.name.count() >= 2)
            it.name.substring(0, 2) else it.name.substring(0, 1)
        val color = ColorGenerator.MATERIAL
        val builder = TextDrawable.Builder(text = text, color = color.getColor(text))
        val dr = TextDrawable(builder)
        binding.logo.setImageDrawable(dr)
        binding.cryptoCurrencyItem.setOnClickListener {
            cryptoCurrencyListAction.onItemClick(cryptoCurrency)
        }
    }
}



