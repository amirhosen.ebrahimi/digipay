package com.digipay.cryptocurrencylist2.presentation.mapper

import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import io.phoenix.businessmessenger.common.mapper.Mapper
import javax.inject.Inject

class CryptoCurrencyToCryptoCurrencyView @Inject constructor() :
    Mapper<CryptoCurrency, CryptoCurrencyView> {
    override fun map(first: CryptoCurrency): CryptoCurrencyView {
        return CryptoCurrencyView(
            id = first.id,
            name = first.name,
            price = first.price,
            marketCap = first.marketCap,
            percentChange = first.percentChange,
            symbol = first.symbol
        )
    }

}