package com.digipay.cryptocurrencylist2

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class CryptoCurrencyScope