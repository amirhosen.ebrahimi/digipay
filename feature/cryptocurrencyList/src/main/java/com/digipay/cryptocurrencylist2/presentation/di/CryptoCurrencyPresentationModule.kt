package com.digipay.cryptocurrencylist2.presentation.di

import com.digipay.cryptocurrencylist2.CryptoCurrencyScope
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import com.digipay.cryptocurrencylist2.presentation.mapper.CryptoCurrencyToCryptoCurrencyView
import dagger.Binds
import dagger.Module
import io.phoenix.businessmessenger.common.di.InjectingSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.di.ViewModelFactory
import io.phoenix.businessmessenger.common.mapper.Mapper

@Module
abstract class CryptoCurrencyPresentationModule {

    @Binds
    @CryptoCurrencyScope
    abstract fun bindCryptoCurrencyToCryptoCurrencyView(mapper: CryptoCurrencyToCryptoCurrencyView): Mapper<CryptoCurrency, CryptoCurrencyView>


    @Binds
    @CryptoCurrencyScope
    abstract fun bindViewModelFactory(mapper: InjectingSavedStateViewModelFactory): ViewModelFactory


}