package com.digipay.cryptocurrencylist2.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.digipay.cryptocurrencylist2.R
import com.digipay.cryptocurrencylist2.presentation.adapter.viewholder.NetworkStateItemViewHolder
import com.digipay.cryptocurrencylist2.presentation.interfaces.RetryAction
import io.phoenix.businessmessenger.designSystem.databinding.NetworkStateItemBinding

class CryptoCurrencyLoadStateAdapter(
    private val onclick: RetryAction
) : LoadStateAdapter<NetworkStateItemViewHolder>() {
    override fun onBindViewHolder(holder: NetworkStateItemViewHolder, loadState: LoadState) {
        holder.bindTo(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): NetworkStateItemViewHolder {
        return NetworkStateItemViewHolder(
            NetworkStateItemBinding.bind(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.network_state_item, parent, false
                )
            ), onclick
        )
    }
}