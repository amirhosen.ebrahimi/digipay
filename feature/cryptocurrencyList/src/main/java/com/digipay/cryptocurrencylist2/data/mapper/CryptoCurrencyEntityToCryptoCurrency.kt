package com.digipay.cryptocurrencylist2.data.mapper

import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import javax.inject.Inject

class CryptoCurrencyEntityToCryptoCurrency @Inject constructor() :
    Mapper<CryptoCurrencyEntity, CryptoCurrency> {
    override fun map(first: CryptoCurrencyEntity): CryptoCurrency {
         return CryptoCurrency(
             id = first.id,
             name = first.name,
             price = first.price,
             marketCap = first.marketCap,
             percentChange = first.percentChange,
             symbol = first.symbol
         )
    }

}