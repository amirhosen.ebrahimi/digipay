package com.digipay.cryptocurrencylist2.domail.entity


data class CryptoCurrency(
    val id: Int,
    val name: String,
    val symbol: String?,
    val price: String?,
    val percentChange: String?,
    val marketCap: String?
)
