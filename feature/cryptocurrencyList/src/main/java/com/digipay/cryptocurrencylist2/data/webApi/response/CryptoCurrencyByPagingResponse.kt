package com.digipay.cryptocurrencylist2.data.webApi.response

import com.google.gson.annotations.SerializedName

data class CryptoCurrencyByPagingResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("username")
    val userName: String,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?,
)