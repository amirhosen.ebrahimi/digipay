package com.digipay.cryptocurrencylist2.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.LoadType.*
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.digipay.cryptocurrencylist2.data.dataSource.*
import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import com.digipay.cryptocurrencylist2.domail.entity.SortType
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import kotlinx.coroutines.flow.first
import javax.inject.Inject

@OptIn(ExperimentalPagingApi::class)
class PageKeyedRemoteMediator @Inject constructor(
    private val cryptoCurrencyItemMapperByPaging: Mapper<List<Data>, MutableList<CryptoCurrencyEntity>>,
    private val localRemoteKeyDataSourceReadable: LocalCryptoCurrencyRemoteKeyDataSourceReadable,
    private val localRemoteKeyDataSourceDeletable: LocalCryptoCurrencyRemoteKeyDataSourceDeletable,
    private val localRemoteKeyDataSourceWritable: LocalCryptoCurrencyRemoteKeyDataSourceWritable,
    private val remoteCryptoCurrencyByPagingDataSourceReadable: RemoteCryptoCurrencyByPagingDataSourceReadable,
    private val localCryptoCurrencyDataSourceWritable: LocalCryptoCurrencyDataSourceWritable,
    private val localCryptoCurrencyDataSourceDeletable: LocalCryptoCurrencyDataSourceDeletable
) : RemoteMediator<Int, CryptoCurrencyEntity>() {
    private lateinit var sortType: SortType

    operator fun invoke(parameters: SortType): PageKeyedRemoteMediator {
        sortType = parameters
        return this
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, CryptoCurrencyEntity>
    ): MediatorResult {
        val loadKey = when (loadType) {
            REFRESH -> {
                CryptoCurrencyRemoteKeyEntity(
                    nextPageKey = firstPage,
                    limit = when (loadType) {
                        REFRESH -> state.config.initialLoadSize
                        else -> state.config.pageSize
                    }
                )
            }
            PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
            APPEND -> {
                localRemoteKeyDataSourceReadable.read().first()
            }
        }

        try {
            val data =
                remoteCryptoCurrencyByPagingDataSourceReadable.read(
                    RemoteCryptoCurrencyByPagingDataSourceReadable.Params(
                        loadKey
                    )
                )
            val dataBody = data.body()

            return if (data.isSuccessful && dataBody != null) {
                if (loadType == REFRESH) {
                    localRemoteKeyDataSourceDeletable.delete(Unit)
                    localCryptoCurrencyDataSourceDeletable.delete(Unit)
                }
                localCryptoCurrencyDataSourceWritable.write(
                    LocalCryptoCurrencyDataSourceWritable.Params(
                        cryptoCurrencyItemMapperByPaging.map(dataBody.data)
                    )
                )
                localRemoteKeyDataSourceWritable.write(
                    loadKey.copy(nextPageKey = loadKey.nextPageKey + nextCounterPage)
                )
                MediatorResult.Success(
                    endOfPaginationReached = dataBody.data.isEmpty()
                )
            } else {
                MediatorResult.Success(
                    endOfPaginationReached = true
                )
            }
        } catch (e: Throwable) {
            return MediatorResult.Error(e)
        }
    }

    companion object {
        const val firstPage = 1
        const val nextCounterPage = 1
    }
}

