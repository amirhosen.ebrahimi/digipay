package com.digipay.cryptocurrencylist2.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.map
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.domail.entity.SortType
import com.digipay.cryptocurrencylist2.domail.usecase.GetCryptoCurrencyUseCase
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import io.phoenix.businessmessenger.common.di.AssistedSavedStateViewModelFactory
import io.phoenix.businessmessenger.common.mapper.Mapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*

class CryptoCurrencyViewModel @AssistedInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val getCryptoCurrencyUseCase: GetCryptoCurrencyUseCase,
    private val cryptoCurrencyToCryptoCurrencyView: Mapper<CryptoCurrency, CryptoCurrencyView>
) : ViewModel() {
    private var sortType = SortType.none

    init {
        savedStateHandle.set(KEY_STATE, sortType)
    }

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    val cryptoCurrencyList = flowOf(
        savedStateHandle.getLiveData<String>(KEY_STATE)
            .asFlow()
            .flatMapLatest {
                getCryptoCurrencyUseCase(
                    GetCryptoCurrencyUseCase.Params(
                        sortType = it,
                        pagingConfig = PagingConfig(
                            pageSize = PAGE_SIZE,
                            initialLoadSize = INITIAL_LOAD_SIZE
                        )
                    )
                )
            }
            .cachedIn(viewModelScope)
    ).flattenMerge(DEFAULT_CONCURRENCY).mapLatest { pagingData ->
        pagingData.map { entity ->
            cryptoCurrencyToCryptoCurrencyView.map(entity)
        }
    }

    fun rGetCryptoCurrency(sortT: String) {
        sortType = sortT
        savedStateHandle.set(KEY_STATE, sortType)
    }

    fun rGetLastCryptoCurrency() {
        savedStateHandle.set(KEY_STATE, sortType)
    }

    @AssistedInject.Factory
    interface Factory : AssistedSavedStateViewModelFactory<CryptoCurrencyViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): CryptoCurrencyViewModel
    }

    companion object {
        const val PAGE_SIZE = 3
        const val INITIAL_LOAD_SIZE = 3
        const val KEY_STATE = "cryptoCurrencyList"
    }
}