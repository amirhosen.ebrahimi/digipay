package com.digipay.cryptocurrencylist2.data.di

import com.digipay.cryptocurrencylist2.CryptoCurrencyScope
import com.digipay.cryptocurrencylist2.data.dataSource.*
import com.digipay.cryptocurrencylist2.data.mapper.CryptoCurrencyEntityToCryptoCurrency
import com.digipay.cryptocurrencylist2.data.mapper.CryptoCurrencyListResponseToCryptoCurrencyEntity
import com.digipay.cryptocurrencylist2.data.repository.CryptoCurrencyRepositoryImpl
import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import com.digipay.cryptocurrencylist2.data.webApi.service.CryptoCurrencyService
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.domail.repository.CryptoCurrencyRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.phoenix.businessmessenger.common.mapper.Mapper
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import retrofit2.Retrofit

@Module
abstract class CryptoCurrencyDataModule {
    //......................Mappers...........................

    @Binds
    @CryptoCurrencyScope
    abstract fun bindCryptoCurrencyEntityToCryptoCurrency(mapper: CryptoCurrencyEntityToCryptoCurrency): Mapper<CryptoCurrencyEntity, CryptoCurrency>

    @Binds
    @CryptoCurrencyScope
    abstract fun bindCryptoCurrencyListResponseToCryptoCurrencyEntity(mapper: CryptoCurrencyListResponseToCryptoCurrencyEntity): Mapper<@JvmSuppressWildcards List<Data>, @JvmSuppressWildcards List<CryptoCurrencyEntity>>


    //......................DataSource...........................

    @Binds
    @CryptoCurrencyScope
    abstract fun bindRemoteCryptoCurrencyDataSourceReadable(dataSource: RemoteCryptoCurrencyByPagingDataSource): RemoteCryptoCurrencyByPagingDataSourceReadable


    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyRemoteKeyDataSourceWritable(dataSource: LocalCryptoCurrencyRemoteKeyDataSource): LocalCryptoCurrencyRemoteKeyDataSourceWritable

    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyLocalCryptoCurrencyRemoteKeyDataSourceReadable(dataSource: LocalCryptoCurrencyRemoteKeyDataSource): LocalCryptoCurrencyRemoteKeyDataSourceReadable

    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyRemoteKeyDataSourceDeletable(dataSource: LocalCryptoCurrencyRemoteKeyDataSource): LocalCryptoCurrencyRemoteKeyDataSourceDeletable

    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyDataSourceReadable(dataSource: LocalCryptoCurrencyDataSource): LocalCryptoCurrencyDataSourceReadable

    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyDataSourceWritable(dataSource: LocalCryptoCurrencyDataSource): LocalCryptoCurrencyDataSourceWritable

    @Binds
    @CryptoCurrencyScope
    abstract fun bindLocalCryptoCurrencyDataSourceDeletable(dataSource: LocalCryptoCurrencyDataSource): LocalCryptoCurrencyDataSourceDeletable


    //......................Repository...........................

    @Binds
    @CryptoCurrencyScope
    abstract fun bindCryptoCurrencyRepositoryImpl(repo: CryptoCurrencyRepositoryImpl): CryptoCurrencyRepository


    companion object {
        @Provides
        @CryptoCurrencyScope
        fun provideApiService(retrofit: Retrofit): CryptoCurrencyService {
            return retrofit.create(CryptoCurrencyService::class.java)
        }
    }

}