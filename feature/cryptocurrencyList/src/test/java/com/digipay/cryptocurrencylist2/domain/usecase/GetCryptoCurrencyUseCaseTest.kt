package com.digipay.cryptocurrencylist2.domain.usecase


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.domail.repository.CryptoCurrencyRepository
import com.digipay.cryptocurrencylist2.domail.usecase.GetCryptoCurrencyUseCase
import com.digipay.cryptocurrencylist2.presentation.CryptoCurrencyViewModel
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockk
import io.phoenix.businessmessenger.common.testshare.MainCoroutineRule
import io.phoenix.businessmessenger.common.testshare.runBlockingTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GetCryptoCurrencyUseCaseTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private lateinit var getCryptoCurrencyUseCase: GetCryptoCurrencyUseCase

    private val cryptoCurrencyRepository = mockk<CryptoCurrencyRepository>(relaxed = true)

    @Before
    fun config() {
        MockKAnnotations.init(this)
        getCryptoCurrencyUseCase = createUseCase()
    }

    @Test
    fun getContactConfigSuccess() = coroutineRule.runBlockingTest {
        every { cryptoCurrencyRepository.getCryptoCurrency(any(), any()) } answers {
            flow {
                PagingData.from(
                    DATA
                )
            }
        }

        getCryptoCurrencyUseCase(
            GetCryptoCurrencyUseCase.Params(
                pagingConfig = PagingConfig(
                    pageSize = CryptoCurrencyViewModel.PAGE_SIZE,
                    initialLoadSize = CryptoCurrencyViewModel.INITIAL_LOAD_SIZE
                )
            )
        ).collect {
            assert(it == DATA)
        }
    }

    private fun createUseCase(): GetCryptoCurrencyUseCase {
        return GetCryptoCurrencyUseCase(
            dispatcher = coroutineRule.testDispatcher,
            repository = cryptoCurrencyRepository
        )
    }

    companion object {
        val ERROR = Exception(Throwable(message = "error"))
        val DATA = listOf(
            CryptoCurrency(
                id = 1,
                name = "name",
                symbol = "symbol",
                price = "price",
                percentChange = "percentChange",
                marketCap = "marketCap"
            )
        )
    }

}
