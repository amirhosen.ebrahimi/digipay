package com.digipay.cryptocurrencylist2.data.dataSrouce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digipay.cryptocurrencylist2.data.dataSource.LocalCryptoCurrencyRemoteKeyDataSource
import io.mockk.MockKAnnotations
import io.phoenix.businessmessenger.common.testshare.MainCoroutineRule
import io.phoenix.businessmessenger.common.testshare.runBlockingTest
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import io.phoenix.businessmessenger.data.pref.FakeSharedPreferenceStorage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class LocalCryptoCurrencyRemoteKeyDataSourceTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private lateinit var localContactRemoteKeyDataSource: LocalCryptoCurrencyRemoteKeyDataSource

    private val fakeSharedPreferenceStorage = FakeSharedPreferenceStorage()

    @Before
    fun config() {
        MockKAnnotations.init(this)
        localContactRemoteKeyDataSource = createDataSource()
    }

    private fun createDataSource(): LocalCryptoCurrencyRemoteKeyDataSource {
        return LocalCryptoCurrencyRemoteKeyDataSource(fakeSharedPreferenceStorage)
    }

    @Test
    fun writeLocalContactRemoteKey() = coroutineRule.runBlockingTest {
        localContactRemoteKeyDataSource.write(CONTACT_REMOTE_KEY)
        val data = fakeSharedPreferenceStorage.cryptoCurrencyRemoteKeyEntity.first()
        assert(data == CONTACT_REMOTE_KEY)
    }

    @Test
    fun readLocalContactRemoteKey() = coroutineRule.runBlockingTest {
        fakeSharedPreferenceStorage.saveCryptoCurrencyRemoteKey(CONTACT_REMOTE_KEY)
        val data = localContactRemoteKeyDataSource.read().first()
        assert(data == CONTACT_REMOTE_KEY)
    }

    @Test
    fun deleteLocalContactRemoteKey() {
        // todo
    }

    companion object {
        val CONTACT_REMOTE_KEY = CryptoCurrencyRemoteKeyEntity(
            nextPageKey = 1,
            limit = 10
        )
    }


}