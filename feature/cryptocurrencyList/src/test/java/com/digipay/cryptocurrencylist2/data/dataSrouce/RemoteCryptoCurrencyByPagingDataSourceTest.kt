package com.digipay.cryptocurrencylist2.data.dataSrouce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digipay.cryptocurrencylist2.data.dataSource.RemoteCryptoCurrencyByPagingDataSource
import com.digipay.cryptocurrencylist2.data.dataSource.RemoteCryptoCurrencyByPagingDataSourceReadable
import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import com.digipay.cryptocurrencylist2.data.webApi.response.Quote
import com.digipay.cryptocurrencylist2.data.webApi.response.USD
import com.digipay.cryptocurrencylist2.data.webApi.service.CryptoCurrencyService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.mockk
import io.phoenix.businessmessenger.common.testshare.MainCoroutineRule
import io.phoenix.businessmessenger.common.testshare.runBlockingTest
import io.phoenix.businessmessenger.data.entity.BaseResponse
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class RemoteCryptoCurrencyByPagingDataSourceTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private lateinit var remoteCryptoCurrencyByPagingDataSource: RemoteCryptoCurrencyByPagingDataSource
    private val cryptoCurrencyService =
        mockk<CryptoCurrencyService>(relaxed = true)

    @Before
    fun config() {
        MockKAnnotations.init(this)
        remoteCryptoCurrencyByPagingDataSource = createDataSource()
    }

    private fun createDataSource(): RemoteCryptoCurrencyByPagingDataSource {
        return RemoteCryptoCurrencyByPagingDataSource(cryptoCurrencyService)
    }

    @Test
    fun getRemoteCryptoCurrency() = coroutineRule.runBlockingTest {
        coEvery {
            cryptoCurrencyService.getCryptoCurrencyByPaging(
                any(),
                any(),
                any(),
                any()
            )
        } coAnswers {
            Response.success(CryptoCurrency_LIST)
        }
        val data = remoteCryptoCurrencyByPagingDataSource.read(
            RemoteCryptoCurrencyByPagingDataSourceReadable.Params(
                cryptoCurrencyRemoteKeyEntity = CryptoCurrencyRemoteKeyEntity()
            )
        )
        assert(data.body() == CryptoCurrency_LIST)
    }

    companion object {
        val CryptoCurrency_LIST = BaseResponse(

            data = listOf(
                Data(
                    id = 1,
                    name = "name",
                    symbol = "symbol",
                    slug = "slug",
                    num_market_pairs = 0,
                    date_added = "date_added",
                    tags = listOf(),
                    max_supply = 0.0,
                    circulating_supply = 0.0,
                    total_supply = 0.0,
                    cmc_rank = 0,
                    last_updated = "last_updated",
                    quote = Quote(
                        USD(
                            0.0,
                            0.0,
                            0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, "last_updated"
                        )
                    )
                )
            )
        )
        val ERROR = Exception(Throwable(message = "error"))
    }
}