package com.digipay.cryptocurrencylist2.data.mapper


import com.digipay.cryptocurrencylist2.data.webApi.response.Data
import com.digipay.cryptocurrencylist2.data.webApi.response.Quote
import com.digipay.cryptocurrencylist2.data.webApi.response.USD
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import org.junit.Test

class CryptoCurrencyListResponseToCryptoCurrencyEntityTest {
    @Test
    fun mapTestingHaveItem() {

        val data = CryptoCurrencyListResponseToCryptoCurrencyEntity().map(
            listOf(
                Data(
                    id = 1,
                    name = "name",
                    symbol = "symbol",
                    slug = "slug",
                    num_market_pairs = 0,
                    date_added = "date_added",
                    tags = listOf(),
                    max_supply = 0.0,
                    circulating_supply = 0.0,
                    total_supply = 0.0,
                    cmc_rank = 0,
                    last_updated = "last_updated",
                    quote = Quote(
                        USD(
                            0.0,
                            0.0,
                            0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, "last_updated"
                        )
                    )
                )
            )
        )
        assert(
            data[0] == listOf(
                CryptoCurrencyEntity(
                    id = 1,
                    name = "name",
                    symbol = "symbol",
                    price = "0.0",
                    percentChange = "0.0",
                    marketCap = "0.0"
                )
            )[0]
        )
        assert(data.size == 1)
    }

    @Test
    fun mapTestingNotHaveThumbnailItem() {

        val data = CryptoCurrencyListResponseToCryptoCurrencyEntity().map(
            emptyList()
        )
        assert(
            data.isEmpty()
        )
    }

}