package com.digipay.cryptocurrencylist2.presentation.mapper

import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import com.digipay.cryptocurrencylist2.presentation.entity.CryptoCurrencyView
import org.junit.Test

class CryptoCurrencyToCryptoCurrencyViewTest {
    @Test
    fun mapTesting() {
        val data = CryptoCurrencyToCryptoCurrencyView().map(
            CryptoCurrency(
                id = 1,
                name = "name",
                price = "price",
                marketCap = "marketCap",
                percentChange = "percentChange",
                symbol = "symbol"
            )
        )
        assert(
            data == CryptoCurrencyView(
                id = 1,
                name = "name",
                price = "price",
                marketCap = "marketCap",
                percentChange = "percentChange",
                symbol = "symbol"
            )
        )
    }
}
