package com.digipay.cryptocurrencylist2.data.mapper

import com.digipay.cryptocurrencylist2.domail.entity.CryptoCurrency
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity
import org.junit.Test

class CryptoCurrencyEntityToCryptoCurrencyTest {
    @Test
    fun mapTesting() {
        val data = CryptoCurrencyEntityToCryptoCurrency().map(
            CryptoCurrencyEntity(
                id = 0,
                name = "name",
                symbol = "symbol",
                price = "price",
                percentChange = "percentChange",
                marketCap = "marketCap"
            )
        )
        assert(
            data == CryptoCurrency(
                id = 0,
                name = "name",
                symbol = "symbol",
                price = "price",
                percentChange = "percentChange",
                marketCap = "marketCap"
            )
        )
    }
}