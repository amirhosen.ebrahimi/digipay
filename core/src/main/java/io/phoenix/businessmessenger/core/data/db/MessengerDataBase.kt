package io.phoenix.businessmessenger.core.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.phoenix.businessmessenger.data.dao.CryptoCurrencyDao
import io.phoenix.businessmessenger.data.dao.CryptoCurrencyDetailDao
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity

@Database(
    entities = [
        CryptoCurrencyEntity::class, CryptoCurrencyDetailEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(LocationTypeConverters::class)
abstract class MessengerDataBase : RoomDatabase() {
    abstract fun getCryptoCurrencyDao(): CryptoCurrencyDao
    abstract fun getCryptoCurrencyDetailDao(): CryptoCurrencyDetailDao
}