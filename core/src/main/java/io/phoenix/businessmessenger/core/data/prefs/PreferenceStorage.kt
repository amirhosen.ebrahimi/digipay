package io.phoenix.businessmessenger.core.data.prefs

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import io.phoenix.businessmessenger.data.entity.toCryptoCurrencyRemoteKey
import io.phoenix.businessmessenger.data.entity.toGson
import io.phoenix.businessmessenger.data.pref.PreferenceStorage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class SharedPreferenceStorage @Inject constructor(
    private val dataStore: DataStore<Preferences>
) : PreferenceStorage {

    override suspend fun saveCryptoCurrencyRemoteKey(prefer: CryptoCurrencyRemoteKeyEntity) {
        dataStore.edit {
            it[PreferencesKeys.CRYPTO_CURRENCY_REMOTE_KEY] = prefer.toGson()
        }
    }

    override var cryptoCurrencyRemoteKeyEntity: Flow<CryptoCurrencyRemoteKeyEntity> =
        dataStore.data.map {
            it[PreferencesKeys.CRYPTO_CURRENCY_REMOTE_KEY]?.toCryptoCurrencyRemoteKey()
                ?: CryptoCurrencyRemoteKeyEntity()
        }

    object PreferencesKeys {
        val CRYPTO_CURRENCY_REMOTE_KEY = stringPreferencesKey("crypto_currency_key")
    }


}
