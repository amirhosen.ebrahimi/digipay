package io.phoenix.businessmessenger.di.module


import com.digipay.cryptocurrencyDetail.CryptoCurrencyDetailScope
import com.digipay.cryptocurrencyDetail.data.di.CryptoCurrencyDetailDataModule
import com.digipay.cryptocurrencyDetail.presentation.CryptoCurrencyDetailFragment
import com.digipay.cryptocurrencyDetail.presentation.di.CryptoCurrencyDetailAssistedModule
import com.digipay.cryptocurrencyDetail.presentation.di.CryptoCurrencyDetailPresentationModule
import com.digipay.cryptocurrencylist2.CryptoCurrencyScope
import com.digipay.cryptocurrencylist2.data.di.CryptoCurrencyDataModule
import com.digipay.cryptocurrencylist2.presentation.CryptoCurrencyFragment
import com.digipay.cryptocurrencylist2.presentation.di.CryptoCurrencyAssistedModule
import com.digipay.cryptocurrencylist2.presentation.di.CryptoCurrencyPresentationModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentBindingModule {
    @CryptoCurrencyScope
    @ContributesAndroidInjector(modules = [CryptoCurrencyPresentationModule::class, CryptoCurrencyAssistedModule::class, CryptoCurrencyDataModule::class])
    internal abstract fun bindCryptoCurrencyFragment(): CryptoCurrencyFragment

    @CryptoCurrencyDetailScope
    @ContributesAndroidInjector(modules = [CryptoCurrencyDetailPresentationModule::class, CryptoCurrencyDetailAssistedModule::class, CryptoCurrencyDetailDataModule::class])
    internal abstract fun bindCryptoCurrencyDetailFragment(): CryptoCurrencyDetailFragment
}