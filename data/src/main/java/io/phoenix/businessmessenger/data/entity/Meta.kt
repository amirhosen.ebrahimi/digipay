package io.phoenix.businessmessenger.data.entity


data class Meta(val code: Int? = null, val requestId: String? = null)
