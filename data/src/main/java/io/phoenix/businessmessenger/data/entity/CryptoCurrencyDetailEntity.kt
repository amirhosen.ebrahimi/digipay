package io.phoenix.businessmessenger.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "crypto_currency_detail")
data class CryptoCurrencyDetailEntity(
    @PrimaryKey val id: Int,
    val name: String,
    val des: String,
    val logo: String
)