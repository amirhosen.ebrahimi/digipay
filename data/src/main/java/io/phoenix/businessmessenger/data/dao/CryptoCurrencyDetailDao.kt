package io.phoenix.businessmessenger.data.dao

import androidx.room.Dao
import androidx.room.Query
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyDetailEntity


@Dao
interface CryptoCurrencyDetailDao : BaseDao<CryptoCurrencyDetailEntity> {
    @Query("SELECT * FROM crypto_currency_detail WHERE (id=:id) ")
    fun getById(id: String): CryptoCurrencyDetailEntity
}
