package io.phoenix.businessmessenger.data.pref

import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import kotlinx.coroutines.flow.Flow


interface PreferenceStorage {

    suspend fun saveCryptoCurrencyRemoteKey(prefer: CryptoCurrencyRemoteKeyEntity)
    val cryptoCurrencyRemoteKeyEntity: Flow<CryptoCurrencyRemoteKeyEntity>


    companion object {
        val PREFS_NAME = "digipay"
    }
}