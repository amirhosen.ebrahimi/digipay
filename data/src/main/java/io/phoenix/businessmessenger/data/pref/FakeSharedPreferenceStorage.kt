package io.phoenix.businessmessenger.data.pref

import io.phoenix.businessmessenger.data.entity.CryptoCurrencyRemoteKeyEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow


class FakeSharedPreferenceStorage : PreferenceStorage {
    override suspend fun saveCryptoCurrencyRemoteKey(prefer: CryptoCurrencyRemoteKeyEntity) {
        _iamConfigEntity.value = prefer
    }

    private val _iamConfigEntity = MutableStateFlow(CryptoCurrencyRemoteKeyEntity())
    override val cryptoCurrencyRemoteKeyEntity: Flow<CryptoCurrencyRemoteKeyEntity> =
        _iamConfigEntity
}
