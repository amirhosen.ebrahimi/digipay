package io.phoenix.businessmessenger.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "crypto_currency")
data class CryptoCurrencyEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val symbol: String?,
    val price: String?,
    val percentChange: String?,
    val marketCap: String?
)