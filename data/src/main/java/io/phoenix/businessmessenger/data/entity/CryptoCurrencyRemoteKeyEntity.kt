package io.phoenix.businessmessenger.data.entity

import com.google.gson.Gson

data class CryptoCurrencyRemoteKeyEntity(
    val nextPageKey: Int = 0,
    val limit: Int = 0
)

fun CryptoCurrencyRemoteKeyEntity.toGson(): String {
    return Gson().toJson(this)
}

fun String.toCryptoCurrencyRemoteKey(): CryptoCurrencyRemoteKeyEntity {
    return Gson().fromJson(this, CryptoCurrencyRemoteKeyEntity::class.java)
}
