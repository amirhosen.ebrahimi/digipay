package io.phoenix.businessmessenger.data.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import io.phoenix.businessmessenger.data.entity.CryptoCurrencyEntity


@Dao
interface CryptoCurrencyDao : BaseDao<CryptoCurrencyEntity> {
    @Query("SELECT * FROM crypto_currency ")
    fun getAll(): PagingSource<Int, CryptoCurrencyEntity>

    @Query("SELECT * FROM crypto_currency order by price asc")
    fun getAllByPrice(): PagingSource<Int, CryptoCurrencyEntity>

    @Query("SELECT * FROM crypto_currency order by name asc")
    fun getAllByName(): PagingSource<Int, CryptoCurrencyEntity>

    @Query("SELECT * FROM crypto_currency order by marketCap asc")
    fun getAllByMarketCap(): PagingSource<Int, CryptoCurrencyEntity>


    @Query("SELECT * FROM crypto_currency ")
    fun getAllList(): List<CryptoCurrencyEntity>

    @Query("DELETE FROM crypto_currency ")
    fun deleteAll()

}
